import 'package:flutter/material.dart';
// import 'package:flutter/services.dart';
// import 'package:new_project/main.dart';
// import 'package:new_project/signup.dart';

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  static const String _title = 'Emirates Logistics';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: /*const Padding(*/
          // padding: EdgeInsets.only(left: 50),
          /*child:*/ const Text(_title, style: TextStyle(fontFamily: 'Times New Roman', fontWeight: FontWeight.bold, color: Color(0xffffcdd2))),
        // ), 
        backgroundColor: Colors.cyan[900]
      ),
      body: const MyStatefulWidget(),
    );
  }
}

class MyStatefulWidget extends StatefulWidget {
  const MyStatefulWidget({Key? key}) : super(key: key);

  @override
  State<MyStatefulWidget> createState() => _MyStatefulWidgetState();
}

class _MyStatefulWidgetState extends State<MyStatefulWidget> {
  TextEditingController nameController = TextEditingController();
  TextEditingController passwordController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Padding(
        padding: const EdgeInsets.all(10),
        child: ListView(
          children: [
            const SizedBox(
              //Use of SizedBox
              height: 50,
            ),
            Container(
                alignment: Alignment.center,
                padding: const EdgeInsets.all(5),
                child: const Text(
                  'Welcome',
                  style: TextStyle(
                      color: Colors.green,
                      fontWeight: FontWeight.w700,
                      fontSize: 30,
                      fontFamily: 'Times New Roman'),
                )),
            Container(
                alignment: Alignment.center,
                padding: const EdgeInsets.all(5),
                child: const Text(
                  'Sign in',
                  style: TextStyle(fontSize: 20, fontFamily: 'Times New Roman'),
                )),
            const SizedBox(
              //Use of SizedBox
              height: 30,
            ),
            Container(
              padding: const EdgeInsets.all(10),
              child: TextField(
                controller: nameController,
                decoration: const InputDecoration(
                  border: OutlineInputBorder(),
                  labelText: 'User Name',
                ),
              ),
            ),
            const SizedBox(
              //Use of SizedBox
              height: 5,
            ),
            Container(
              padding: const EdgeInsets.fromLTRB(10, 10, 10, 0),
              child: TextField(
                obscureText: true,
                controller: passwordController,
                decoration: const InputDecoration(
                  border: OutlineInputBorder(),
                  labelText: 'Password',
                ),
              ),
            ),
            // TextButton(
            //   onPressed: () {
            //     //forgot password screen
            //   },
            //   child: const Text('Forgot Password',),
            // ),
            const SizedBox(
              //Use of SizedBox
              height: 30,
            ),
            Container(
                height: 50,
                padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
                child: ElevatedButton(
                  child: const Text('Login'),
                  onPressed: () {
                    print(nameController.text);
                    print(passwordController.text);
                  },
                )),
            const SizedBox(
              //Use of SizedBox
              height: 15,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                const Text(
                  'Does not have account?',
                  style: TextStyle(fontFamily: 'Times New Roman', fontSize: 15),
                ),
                TextButton(
                  child: const Text(
                    'Sign up',
                    style:
                        TextStyle(fontFamily: 'Times New Roman', fontSize: 20),
                  ),
                  onPressed: () {
                    //signup screen
                    Navigator.pushNamed(context, 'signup');
                  },
                )
              ],
            ),
          ],
        ));
  }
}
